extends Node2D

var finished = false

func _input(event):
	if event.is_action_pressed("rewind") && finished:
		var boss = get_node("../..")
		boss.start_big_rewind()
		boss.player.position = boss.player_positions[59]
		#for u in range(61):
		for u in range(2):
			boss.rewind(-1)
		boss.stop_rewind()
		boss.forced_delay = true
		finished = false
		$AnimatedSprite.hide()

func start():
	get_tree().paused = true
	get_node("../../sounds/explosion2").play()
	$AnimatedSprite.show()
	$AnimatedSprite.frame = 0
	$AnimatedSprite.play("default")
	

func _on_AnimatedSprite_animation_finished():
	finished = true
