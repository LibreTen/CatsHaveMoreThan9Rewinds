extends Node2D

func _input(event):
	if event.is_action_pressed("right"):
		change_frame(1)
	elif event.is_action_pressed("left"):
		change_frame(-1)
	elif event.is_action_pressed("start"):
		if $Cutscene.frame == $Cutscene.hframes-1:
			get_tree().change_scene("res://Level.tscn")

func change_frame(i):
	var old_frame = $Cutscene.frame
	$Cutscene.frame +=i
	get_node("Text/"+str(old_frame)).hide()
	get_node("Text/"+str($Cutscene.frame)).show()
