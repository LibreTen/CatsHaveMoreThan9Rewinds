extends Node2D

onready var TextBox = get_node("/root/Level/CanvasLayer/Textbox")

func ready():
	pass
	#TextBox = get_node("TextArea")

func show_box(text):
	TextBox.get_node("text").text = text
	#$CanvasLayer/Dialog/text.add_color_override("font_color_shadow", c)
	TextBox.show()

func hide_box():
	TextBox.hide()

