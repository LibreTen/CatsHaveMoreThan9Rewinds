extends TileMap
func _ready():
	for ts in tile_set.get_tiles_ids():           # for every textures in tileset.
		if tile_set.tile_get_shapes(ts).empty():  # if texture didn't have any collision shapes.
			var shape = ConvexPolygonShape2D.new()
			shape.set_points([Vector2(0, 0), Vector2(16, 0), Vector2(16, 16), Vector2(0, 16)])
			var c = tile_set.tile_get_texture(ts).get_width()  / int(cell_size.x)  # colums
			var r = tile_set.tile_get_texture(ts).get_height() / int(cell_size.y)  # rows
			for i in range(c * r):
				tile_set.tile_add_shape(ts, shape, Transform2D(), false, Vector2(i % c, i / c))
			tile_set.tile_set_shape(ts, ts, shape)
