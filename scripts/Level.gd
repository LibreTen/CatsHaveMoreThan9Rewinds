extends Node2D

var temp_dict
var just_rewinded := false

#animation vars
var player_target_distance : float
var marker_start_position :float
var former_time_left :float
var player_old_position : Vector2
#export var animation_softness :Curve

var small_rewinding = false
var big_rewinding = false
var current_state := {}

#ANIMATIONS
var pointer :int
var present_point :int
var target :=[]
var player_positions :=[]
var target_positions :=[]
var time_animation
var present_pointer

var ending = false

onready var player := $Player

#TIMERS
onready var SnapShotDelay : Timer = $Timers/SnapShotDelay
onready var RewindingTime : Timer = $Timers/RewindingTime
onready var TimeLimit : Timer = $Timers/TimeLimit 
onready var SecondCounter : Timer = $Timers/SecondCounter

var forced_delay = false
func _input(event):
	if (event.is_action_pressed("right") || event.is_action_pressed("left") ||event.is_action_pressed("down") ||event.is_action_pressed("up")) && big_rewinding:
		$Timers/AutoType.start()
	
	forced_delay = false
	if event.is_action_pressed("rewind") && big_rewinding:
		stop_rewind()
		forced_delay = true
	
	if event.is_action_pressed("rewind") && !big_rewinding && !forced_delay && !$CanvasLayer/GameOver/AnimatedSprite.visible && !ending:
		start_big_rewind()

# Called when the node enters the scene tree for the first time.
func _ready():
	SnapShotDelay.wait_time = GC.TIME_INTERVAL
	TimeLimit.wait_time = GC.TIME_LIMIT
	SnapShotDelay.start()
	TimeLimit.start()
	SecondCounter.start()
	take_snapshot(true)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if ending:
		$CanvasLayer/Fade.modulate += Color(0,0,0,1)*delta
		if $CanvasLayer/Fade.modulate.a >= 1:
			get_tree().change_scene("res://End.tscn")
	
	if (!Input.is_action_pressed("right") && !Input.is_action_pressed("left")&& !Input.is_action_pressed("up")&& !Input.is_action_pressed("down")) || !big_rewinding:
		$Timers/AutoType.stop()
		$Timers/AutoType.wait_time = 0.5
	
	
	if big_rewinding:
		anim_big_rewind(delta)
	else:
		
		$CanvasLayer/hud/Clock/Display.text = "%01d %02d" % [floor(ceil(TimeLimit.time_left)/60), int(ceil(TimeLimit.time_left)) % 60]
	
		$CanvasLayer/hud/Pointer2.position.x = 10.5+floor(GC.TIME_LIMIT-TimeLimit.time_left)
		$CanvasLayer/hud/PointerGhost.position = $CanvasLayer/hud/Pointer2.position

func take_snapshot(send = true,custom_time = -1):
	just_rewinded = false
	var time
	if custom_time ==-1:
		time = floor(TimeLimit.time_left)
	else:
		time = custom_time
	var snapshot = {
			"position":player.position,
			"movex":player.move.x,
			"movey":player.mov.y,
			"movey2":player.move.y,
			"jumps":player.jumps,
			"time_left":time,
			"anim_name":$Player/AnimatedSprite.animation,
			"anim_frame":$Player/AnimatedSprite.frame,
			"items":player.physical_items,
			"jetpack":player.jetpack_active,
			"music_playing": $bgm.music_playing,
			"light_on":player.light_on,
			"fast_music":$bgm.fast
		}
	#for u in $doors.get_children():
	#	snapshot["door_"+u.name] = u.is_open
	for u in $Objects.get_children():
		for p in u.get_children():
			snapshot[u.name+"/"+p.name] = p.state
	if send:
		GC.take_snapshot(snapshot)
	return snapshot


func start_big_rewind():
	get_tree().paused = true
	big_rewinding = true
	marker_start_position = $CanvasLayer/hud/Pointer2.position.x
	
	player_positions = []
	target = []
	target_positions = []
	
	for u in GC.snapshots:
		player_positions.append(u.position)
		target.append(u.time_left)
		target_positions.append(10.5+GC.TIME_LIMIT-u.time_left)
	time_animation = TimeLimit.time_left
	target.append(TimeLimit.time_left)
	target_positions.append(10.5+GC.TIME_LIMIT-target.back())
	player_positions.append(player.position)
	pointer = target.size()-1
	present_point = pointer
	for u in range(target.size()-1,60):
		target.append(GC.TIME_LIMIT-u*GC.TIME_INTERVAL)
		target_positions.append(10.5+GC.TIME_LIMIT-target.back())
		player_positions.append(player.position)
	
	$PlayerGhost.position = $Player.position
	$PlayerGhost.show()
	$PlayerGhost.texture = get_node("Player/AnimatedSprite").frames.get_frame(get_node("Player/AnimatedSprite").animation,get_node("Player/AnimatedSprite").frame)
	current_state = take_snapshot(false)

func anim_big_rewind(delta):
	if Input.is_action_just_pressed("right"):
		rewind(1)
	if Input.is_action_just_pressed("left"):
		rewind(-1)
	if Input.is_action_just_pressed("up"):
		for i in range(6):
			rewind(1)
	if Input.is_action_just_pressed("down"):
		for i in range(6):
			rewind(-1)
	if pointer == target.size()-1:
		$CanvasLayer/hud/Pointer2.position.x =0.5 +floor(target_positions[pointer] * 0.5 + $CanvasLayer/hud/Pointer2.position.x * 0.5)
	else:
		$CanvasLayer/hud/Pointer2.position.x =target_positions[pointer] * 0.5 + $CanvasLayer/hud/Pointer2.position.x * 0.5
	time_animation = target[pointer] * 0.8 + floor(time_animation) * 0.2
	$CanvasLayer/hud/Clock/Display.text = "%01d %02d" % [floor(ceil(time_animation)/60), int(ceil(time_animation)) % 60]
	player.position = player_positions[pointer]
	$PlayerGhost.position = $PlayerGhost.position.linear_interpolate(player_positions[pointer],0.2)

func rewind(i):
	var future = false
	
	var old_pointer = pointer
	pointer +=i
	pointer = clamp(pointer,0,60)
	var snapshot
	if old_pointer<pointer:
		$Objects.update_multiple_times(target[old_pointer],target[pointer])
	if pointer > present_point:
		if old_pointer<pointer:
			snapshot = take_snapshot(true,GC.TIME_LIMIT-(pointer-1)*GC.TIME_INTERVAL)#current_state.duplicate()
		else:
			snapshot = GC.snapshots[pointer-1]
			GC.snapshots = GC.snapshots.slice(0,pointer-1)
		future = true
		#snapshot.time_left = GC.TIME_LIMIT-(pointer-1)*GC.TIME_INTERVAL
	elif pointer == present_point:
		snapshot = current_state
		if old_pointer>pointer:
			GC.snapshots = GC.snapshots.slice(0,pointer-1)
	else:
		snapshot = GC.snapshots[pointer]
	
	#if old_pointer>pointer:# && pointer <= present_point:
	for u in $Objects.get_children():
		for p in u.get_children():
			p.state = snapshot[u.name+"/"+p.name]
	
	TimeLimit.wait_time = snapshot.time_left
	player.move_and_slide(Vector2.ZERO)
	player.move.x = snapshot.movex
	player.mov.y = snapshot.movey
	player.move.y = snapshot.movey2
	player.jumps = snapshot.jumps
	player.set_items(snapshot.items)
	player.light_on = snapshot.light_on
	player.get_node("Lamp").visible = snapshot.light_on
	player.jetpack_active = snapshot.jetpack
	$bgm.change_music(snapshot.music_playing)
	$bgm.fast_music(snapshot.fast_music)
	$Player/AnimatedSprite.animation = snapshot.anim_name
	$Player/AnimatedSprite.frame = snapshot.anim_frame
	#Reload variables

func stop_rewind():
	get_tree().paused = false
	$PlayerGhost.hide()
	big_rewinding = false
	if pointer == present_point:
		return
	elif pointer > present_point:
		pass
		#for u in range(present_point,pointer):
			#take_snapshot(true,GC.TIME_LIMIT-u*GC.TIME_INTERVAL)
		#GC.snapshots.remove(present_point)
		GC.snapshots = GC.snapshots.slice(0,pointer-1)
	else:
		GC.snapshots = GC.snapshots.slice(0,pointer)
	SnapShotDelay.start()
	TimeLimit.start()
	SecondCounter.start()
	just_rewinded = true
	

func _on_SnapShotDelay_timeout():
	take_snapshot(true)


func _on_TimeLimit_timeout():
	$CanvasLayer/GameOver.start()


func _on_RewindingTime_timeout():
	pass
	#rewind()


func _on_SecondCounter_timeout():
	$Objects.update_time(round(TimeLimit.time_left))
	if round(TimeLimit.time_left) < 31:
		$sounds/tick.play()
	#for u in $doors.get_children():
	#	u.time(ceil(TimeLimit.time_left))



func _on_AutoType_timeout():
	$Timers/AutoType.wait_time = 0.05
	$Timers/AutoType.start()
	if Input.is_action_pressed("up"):
		for i in range(6):
			rewind(1)
	elif Input.is_action_pressed("down"):
		for i in range(6):
			rewind(-1)
	elif Input.is_action_pressed("right"):
		rewind(1)
	elif Input.is_action_pressed("left"):
		rewind(-1)


func _on_end_body_entered(body):
	ending = true


func _on_Timer_timeout():
	$CanvasLayer/Fade.modulate = Color(1,1,1,0)
