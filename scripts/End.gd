extends Node2D

var cutscene_finished = false

func _input(event):
	if event.is_action_pressed("ui_cancel") && cutscene_finished:
		get_tree().quit()

func _ready():
	$AnimatedSprite.play()


func _on_AnimatedSprite_animation_finished():
	$Label.show()
	$Timer.start()


func _on_Timer_timeout():
	$Label.hide()
	$Label2.show()
	cutscene_finished = true
