extends Node2D


func _ready():
	
	pass#debug()

func debug():
	GC.blueprints[0] = true
	GC.blueprints[1] = true
	GC.blueprints[2] = true
	GC.blueprints[3] = true
	GC.security_access = 4

#func _input(event):
#	if event.is_action_pressed("up"):
#		if get_node("../Replicator").get_overlapping_bodies().size()>0:
#			get_parent().player.set_items(GC.blueprints.duplicate())



func activate(id,state):
	get_node("Activatables/"+str(id)).activate(state)

func update_time(time):
	for u in $TimedEvents.get_children():
		if u.activate_time == time:
			u.activate()

func update_multiple_times(time1,time2):
	for time in range(floor(time1-0.01),time2-1,-1):
		for u in $TimedEvents.get_children():
			if u.activate_time == time:
				u.activate()

func explode():
	get_node("../CanvasLayer/Fade").modulate = Color.white
	get_node("../CanvasLayer/Fade/Timer").start()
	get_node("/root/Level/sounds/explosion").play()
	for u in $Explosion1.get_children():
		u.hide()
		u.activate(false)

func speedrun_end(t):
	if t != 59:
		get_node("Activatables/3").activate(true)
		get_node("Activatables/4").activate(true)
	else:
		for u in $LastMinute.get_children():
			u.hide()
			u.activate(false)


func rewind():
	pass
