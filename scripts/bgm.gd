extends Node2D

var music_playing = 1
var fast = false setget fast_music
#var dark = 0

func change_music(id):
	if music_playing == id:
		return
	
	$FadeOut.interpolate_property(get_node(str(music_playing)),"volume_db",0,-80,0.5)
	$FadeOut.interpolate_property(get_node(str(id)),"volume_db",-80,0,0.5)
	$FadeOut.start()
	get_node(str(id)).play(get_node(str(music_playing)).get_playback_position())
	music_playing = id

func _on_FadeOut_tween_all_completed():
	for u in range(1,5):
		if u != music_playing:
			get_node(str(u)).stop()

func enter_dark():
	#dark +=1
	#if dark >0 && get_node("1").bus == "Master":
	if get_node("2").bus == "Master":
		for u in range(2,4):
			get_node(str(u)).bus = "Dark"

func exit_dark():
	#dark -=1
	#if dark <0 && 
	if get_node("2").bus == "Dark":
		for u in range(2,4):
			get_node(str(u)).bus = "Master"

func fast_music(value):
	fast = value
	if value:
		get_node("1").pitch_scale =2
		get_node("2").pitch_scale =2
		get_node("3").pitch_scale =2
		get_node("4").pitch_scale =2
	else:
		get_node("1").pitch_scale =1
		get_node("2").pitch_scale =1
		get_node("3").pitch_scale =1
		get_node("4").pitch_scale =1
