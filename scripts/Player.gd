extends KinematicBody2D


var max_jumps = 1
var mov = Vector2.ZERO
var walk_speed := 6000
var rewinding = false
var jumps = 0
var slide = 1
var jump_force = -10000
var fall_speed = 20000
var time_factor:=1.0
var move := Vector2.ZERO
var gravity_dir = 1
var wall_grabbing = false
var light_on = false
var anti_gravity_count = 0
var has_gravity = true

var jetpack_active = false
var jetpack_speed_y = 25000
var jetpack_speed_x = 6000

var physical_items := [false,false,false,false]

var side_right = true
var side_up = true


func _input(event):
	if event.is_action_pressed("light") && physical_items[2]:
		$light.play()
		light_on = !light_on
	if event.is_action_pressed("jetpack") && physical_items[3]:
		$light.play()
		jetpack_active = !jetpack_active

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#$Lamp.visible = light_on

func _process(delta):
	if jetpack_active && (!is_on_floor() || abs(move.x) > 5000) && !wall_grabbing:
		$jetpack.play()
	else:
		$jetpack.stop()
	
	
	
	has_gravity = anti_gravity_count==0
	$Lamp.visible = light_on
	$Label.text = str(mov.y)
	
	
	if mov.x<0:
		side_right = false
	elif mov.x>0:
		side_right = true
	
	
	
	var lamp_slide = Input.get_action_strength("down")-Input.get_action_strength("up")
	$Lamp.rotation_degrees = -45 * (0.5-lamp_slide) + 45* (0.5+lamp_slide)
	
	if side_right && lamp_slide == 0:
		$Lamp.rotation = 0
	elif lamp_slide == 0:
		$Lamp.rotation_degrees = 180
	
	var anim
	
	if !wall_grabbing:
		if !is_on_floor() && side_right:
			anim ="jumpr"
		elif !is_on_floor():
			anim ="jumpl"
		elif mov.x>0:
			anim ="runr"
		elif mov.x<0:
			anim ="runl"
		elif side_right:
			anim ="idler"
		else:
			anim ="idlel"
	
	
#	if wall_grabbing && side_right:
#		$AnimatedSprite.rotation_degrees = -90
#	elif wall_grabbing:
#		$AnimatedSprite.rotation_degrees = 90
#	else:
#		$AnimatedSprite.rotation_degrees = 0
#		side_up = true
	
	if wall_grabbing && test_move(transform,Vector2.RIGHT):
		$AnimatedSprite.rotation_degrees = -90
		if  mov.y <0:
			side_up = true
			anim ="runr"
		elif  mov.y>0:
			side_up = false
			anim ="runl"
		elif side_up:
			anim ="idler"
		else:
			anim ="idlel"
	elif wall_grabbing:
		$AnimatedSprite.rotation_degrees = 90
		if  mov.y <0:
			side_up = true
			anim ="runl"
		elif  mov.y>0:
			side_up = false
			anim ="runr"
		elif side_up:
			anim ="idlel"
		else:
			anim ="idler"
	else:
		$AnimatedSprite.rotation_degrees = 0
		side_up = true
	
	if jetpack_active:
		$AnimatedSprite.play(anim+"r")
	else:
		$AnimatedSprite.play(anim)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if rewinding:
		move_and_slide(Vector2.ZERO,Vector2.UP)
		return
	
	mov.x = (Input.get_action_strength("right")-Input.get_action_strength("left"))*walk_speed
	
	if(is_on_floor()):
		jumps = 0
		if has_gravity:
			mov.y = 200 #- get_floor_normal().y*1000
		#mov.x -= get_floor_normal().x*1000
	else:
		if jumps ==0:
			jumps = 1
		mov.y += fall_speed*delta*time_factor
	
	if is_on_wall() && !wall_grabbing:
		move.x = 0
	
	if is_on_ceiling() && has_gravity:
		mov.y = 1000
	
	if !has_gravity:
		mov.y = move.y
		if is_on_floor() && mov.y>100:
			mov.y = 100
	
	if is_on_ceiling() && !has_gravity:
		if mov.y < -100:
			mov.y = -100
		if Input.is_action_just_pressed("jump"):
			$jump.play()
			mov.y = -jump_force*2
	
	if !wall_grabbing && Input.is_action_just_pressed("jump") && jumps <max_jumps && (has_gravity || is_on_floor()):
		#$sound_jump.play()
		jumps+=1
		$jump.play()
		mov.y = jump_force
		#get_node("../sounds/jump").play()
	
	if physical_items[1] && !is_on_floor() && is_on_wall():
		wall_grabbing = true
	
	if (test_move(transform,Vector2.RIGHT) && !Input.is_action_pressed("right")) || (test_move(transform,Vector2.LEFT) && !Input.is_action_pressed("left")):
		wall_grabbing = false
		if !has_gravity && !jetpack_active:
				wall_grabbing = true
	
	if wall_grabbing:
		jumps=1
		mov.x = 0
		mov.y = (Input.get_action_strength("down")-Input.get_action_strength("up"))*6000
		if Input.is_action_just_pressed("jump"):
			$jump.play()
			if !has_gravity:
				mov.y = jump_force* (Input.get_action_strength("up")-Input.get_action_strength("down"))
			else:
				mov.y = jump_force
			wall_grabbing = false
			#side_right = !side_right
			#if Input.is_action_pressed("down"):
				#mov.y =-jump_force
				#side_right = !side_right
			wall_grab_jump()
		if (test_move(transform,Vector2.RIGHT) && !Input.is_action_pressed("right")) || (test_move(transform,Vector2.LEFT) && !Input.is_action_pressed("left")):
			wall_grabbing = false
			if !has_gravity&& !jetpack_active:
				wall_grabbing = true
		elif (!test_move(transform,Vector2.RIGHT) && !test_move(transform,Vector2.LEFT)) || !physical_items[1]:
			wall_grabbing = false
	
	
	if has_gravity&& jetpack_active && !wall_grabbing:
		var jetpack_movement = Vector2(Input.get_action_strength("right")-Input.get_action_strength("left"),Input.get_action_strength("down")-Input.get_action_strength("up"))
		move.x += jetpack_movement.x*jetpack_speed_x*delta
		slide = 0.01
		fall_speed = 10000
		mov.y +=jetpack_movement.y*jetpack_speed_y*delta
	else:
		#slide = 0.6
		fall_speed = 20000
	slide = slide + (1 - slide) * 0.01*delta
	if has_gravity || get_slide_count() >0 || wall_grabbing:
		move.y=mov.y*gravity_dir
		move = move.linear_interpolate(Vector2(mov.x,move.y),slide*time_factor)
	
	if jetpack_active && !wall_grabbing && !has_gravity:
		var jetpack_movement = Vector2(Input.get_action_strength("right")-Input.get_action_strength("left"),Input.get_action_strength("down")-Input.get_action_strength("up"))
		move+= jetpack_movement*jetpack_speed_x*delta
		#mov.y +=jetpack_movement.y*jetpack_speed*delta*100
	var velocity=move_and_slide(move*delta*time_factor,Vector2.UP*gravity_dir,true)

func wall_grab_jump():
	var multiplier = 5
	if !has_gravity:
		#mov.y = 0
		multiplier = 2
	if test_move(transform,Vector2.RIGHT):
		move.x += -walk_speed*multiplier
		slide = 0.1
	else:
		move.x += walk_speed*multiplier
		slide = 0.1
	

func set_items(blueprints):
	physical_items = blueprints
	if physical_items[0]: #double jump
		max_jumps = 2
	else:
		max_jumps = 1

func start_rewinding():
	rewinding = true
	collision_layer = 0

func stop_rewinding():
	rewinding = false
	collision_layer = 1
