extends Node

var TIME_INTERVAL = 5
var TIME_LIMIT = 300 #seconds
var snapshots := []
var blueprints := [false,false,false,false]
var security_access = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func take_snapshot(dict):
	snapshots.append(dict)


func rewind_single():
	return snapshots.back()

func rewind_double():
	if snapshots.size() == 1:
		return snapshots[0]
	snapshots.pop_back()
	return snapshots.back()

func pickup(item_id):
	if item_id<0:
		if security_access< -item_id:
			security_access = -item_id
	else:
		blueprints[item_id] = true
