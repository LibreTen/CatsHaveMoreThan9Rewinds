extends Light2D

export var state = false setget set_state

func activate(value):
	set_state(value)

func set_state(value):
	state = value
	if state:
		show()
	else:
		hide()
