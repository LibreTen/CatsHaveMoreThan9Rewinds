extends Area2D

export var id = 0
var state setget set_state

func _input(event):
	if event.is_action_pressed("up") && get_overlapping_bodies().size()!=0:
		print($AnimatedSprite.animation)
		if id == 3 && $AnimatedSprite.animation == "default":
			$AnimatedSprite.play("activate")
		elif id != 3:
			get_node("../..").activate(id,true)
			set_state(true)
			$light.play()

# Called when the node enters the scene tree for the first time.
func _ready():
	if id == 3:
		$Sprite.hide()
		$AnimatedSprite.show()


func set_state(s):
	state = s
	if id == 3 && state:
		if $AnimatedSprite.animation != "activate":
			$AnimatedSprite.play("activate")
			$AnimatedSprite.frame = 6
	elif id==3:
		$AnimatedSprite.play("default")
	elif state:
		$Sprite.frame = 1
	else:
		$Sprite.frame = 0


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "activate":
		$AnimatedSprite.stop()
		get_node("../..").activate(6,false)
		get_node("../..").activate(7,false)
		get_node("../..").activate(8,false)
		get_node("../../../bgm").fast_music(true)
		state = true


func _on_AnimatedSprite_frame_changed():
	if id ==3 && $AnimatedSprite.animation == "activate" && $AnimatedSprite.playing:
		$light.play()
