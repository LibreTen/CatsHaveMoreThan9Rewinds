extends Area2D

export var text :String
export var hidden = false

func _ready():
	if hidden:
		$Sprite.hide()
	#get_parent().show_box(text)


func _on_TextArea_body_entered(body):
	get_parent().show_box(text)


func _on_TextArea_body_exited(body):
	get_parent().hide_box()
