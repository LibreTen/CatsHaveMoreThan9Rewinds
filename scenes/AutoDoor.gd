extends Area2D


export var state = false setget set_lock #locked
export var security_level = 0
export var overlay_id = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	$StaticBody2D.collision_layer = 1
	$StaticBody2D.collision_mask = 1
	
	
	if !state:
		$Lock.hide()
	elif overlay_id ==1:
		$Lock.hide()
		$Lock2.show()
	
	
	match security_level:
		1:
			$overlay.modulate = Color.lightblue
		2:
			$overlay.modulate = Color.green
		3:
			$overlay.modulate = Color.yellow
		4:
			$overlay.modulate = Color.red
	
	
#	if security_level >0:
#		$StaticBody2D.collision_layer = 1
#		$StaticBody2D.collision_mask = 1
#	if !state:
#		$StaticBody2D.collision_layer = 0
#		$StaticBody2D.collision_mask = 0


func set_lock(locked): #LOCK ONLY
	state = locked
	if locked:
		show()
		$StaticBody2D.collision_layer = 1
		$StaticBody2D.collision_mask = 1
		$Lock.show()
		if overlay_id ==1:
			$Lock.hide()
			$Lock2.show()
	else:
		$Lock.hide()
		$Lock2.hide()
		$StaticBody2D.collision_layer = 0
		$StaticBody2D.collision_mask = 0

func activate(lock):
	set_lock(lock)

func _on_AutoDoor_body_entered(body):
	var security_access = GC.security_access
	if (security_access >= security_level && !state) || (!state && overlay_id == 1):
		$AnimatedSprite.play("open")
		if visible:
			$openclose.play()
		$overlay.play("open")
		$StaticBody2D.collision_layer = 0
		$StaticBody2D.collision_mask = 0


func _on_AutoDoor_body_exited(body):
	if visible && (GC.security_access >= security_level && !state) || (!state && overlay_id == 1):
		$openclose.play()
	$AnimatedSprite.play("close")
	$overlay.play("close")
	#var security_access = get_node("../..").security_access
	$StaticBody2D.collision_layer = 1
	$StaticBody2D.collision_mask = 1
