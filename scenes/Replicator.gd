extends Area2D

var printed2 = false

func _input(event):
	if event.is_action_pressed("up") && get_overlapping_bodies().size()!=0:
		if get_overlapping_bodies().front().physical_items != GC.blueprints.duplicate():
			if printed2:
				get_overlapping_bodies().front().set_items(GC.blueprints.duplicate())
				$AudioStreamPlayer2D2.play()
				$AnimatedSprite.play("stopped")
				$AnimatedSprite.stop()
				printed2 = false
			else:
				if $AnimatedSprite.playing == false:
					$AudioStreamPlayer2D.play()
				$AnimatedSprite.play("printing")
				


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "stopped":
		return
	printed2 = true

func _process(delta):
	pass
