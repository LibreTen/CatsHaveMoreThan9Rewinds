extends StaticBody2D

export var activate_time : int
export var start_open = false
var state = false setget open

# Called when the node enters the scene tree for the first time.
func _ready():
	if start_open:
		open_door()


func open_door():
	state= true
	collision_layer = 0
	collision_mask = 0
	$Sprite.hide()

func close_door():
	state = false
	collision_layer = 1
	collision_mask = 1
	$Sprite.show()

func time(time:int):
	if time == activate_time:
		if state:
			close_door()
		else:
			open_door()

func open(value):
	if value:
		open_door()
	else:
		close_door()

#func rewind(past_time,future_time,reverse):
#	$Label.text = str(past_time) +" "+str(activate_time) +" "+str(future_time)
#	if activate_time < (past_time) && activate_time >= (future_time):
#		open(start_open!=reverse)

func activate():
	print("toggle")
	open(!start_open)
