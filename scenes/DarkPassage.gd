extends Area2D

export var entrance = true



func _on_DarkPassage_body_entered(body):
	if entrance:
		get_node("../..").enter_dark()
	else:
		get_node("../..").exit_dark()
