extends Node2D

export var activate_time : int
export var id = 0
var state = false setget open

func open(value):
	state = value

#func rewind(past_time,future_time,reverse):
#	$Label.text = str(past_time) +" "+str(activate_time) +" "+str(future_time)
#	if activate_time < (past_time) && activate_time >= (future_time):
#		open(start_open!=reverse)

func activate():
	match id:
		0:
			get_node("../..").explode()
		1:
			get_node("../..").speedrun_end(activate_time)
		2:
			get_node("../..").speedrun_end(activate_time)
