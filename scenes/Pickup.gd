extends Area2D

#IMPORTANT, -1,-2,-3,-4 are security keys, while 0 onwards are physical upgrades
export var item_id = 0

func _ready():
	match item_id:
		0:
			$a1.show()
			$Sprite.hide()
		1:
			$b2.show()
			$Sprite.hide()
		2:
			$b3.show()
			$Sprite.hide()
		3:
			$b4.show()
			$Sprite.hide()
		-1:
			modulate = Color.lightblue
		-2:
			modulate = Color.green
		-3:
			modulate = Color.yellow
		-4:
			modulate = Color.red
		


func _on_Pickup_body_entered(body):
	GC.pickup(item_id)
	get_node("/root/Level/sounds/pickup").play()
	queue_free()
