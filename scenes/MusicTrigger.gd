extends Area2D

export var music_id = 0

func _on_MusicTrigger_body_entered(body):
	get_parent().change_music(music_id)
